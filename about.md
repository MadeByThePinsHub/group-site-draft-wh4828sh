---
layout: page
title: About
permalink: /about/
---

For customizing tyour Jekyll-powered site as well as basic usage, visit the [Jekyll website](http://jekyllrb.com/).

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

Built with love and code by [the Pins team](https://gitlab.com/MadeByThePinsTeam-DevLabs/madebythepinsteam-devlabs.gitlab.io) with
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
